# EHU-Health-CMS README
If you are wanting to use the current setup of the CMS for the EHU-Health app on a remote environment, follow the instructions for requirements, installation, and getting started below. The local version of the CMS can be accessed at **[https://bitbucket.org/walshd/ehu-health-cms/overview](https://bitbucket.org/walshd/ehu-health-cms/overview)**, while the frontend of the app can be accessed at **[https://bitbucket.org/walshd/healthplacements/overview](https://bitbucket.org/walshd/healthplacements/overview)**.

The requirements and installation for setting up the CMS for the EHU-Health app can depend on what version of PHP you have installed. If you are running 5.6, then continue reading. Otherwise, go straight to **Getting Started and Plugins** in the **Installation** step after visiting the **[October CMS documentation](https://octobercms.com/docs/setup/installation)** to download the wizard installation that you will upload on the server side. For example, **https://yourdomain.com/install-master** or **https://yourdomain.com/install-master/install.php)**

## Requirements
- The minimum system requirements can be found on the **[October CMS documentation](https://octobercms.com/docs/setup/installation#system-requirements)**.
- Composer will also need to be installed. This will also tell you what packages need to be installed.

## Installation
### CMS on a remote server
1. Clone or download the Git repository locally
2. **```cd```** into the project root.
3. Run **```composer install```**
    1. If you receive an error such as "*Your requirements could not be resolved to an installable set of packages*", then read what the 1st problem is and then try to install that required package.
    2. If ones of the errors state that you require PHP 7 or greater, then try using the following composer.json file instead from the **[octobercms library GitHub repository](https://github.com/octobercms/library/blob/v1.0.419/composer.json)**. Although this hasn't been tested yet, it seems that the reason it might ask for PHP 7 or greater when installing a version with PHP 5.6. This is due to october/rain using certain dependencies, such as doctrine/dbal, that are not updated to the old versions that use PHP 5.6. Once you have replaced the composer.json file with the Github one, remove the vender folder *and maybe even the modules folder*, as well as the composer.lock file.
    3. Repeat step **3**.
4. Upload the project to your remote server.
5. Create a MySQL database on the remote server.
6. If an **.env** file does not exist, run **```php artisan october:env```** on either local or the remote server to generate one.
7. Open the **.env** file and adjust the **DB** settings. You may also have to adjust the **APP_URL** and **APP_KEY**. **APP_URL** is the location of the CMS on the remote server, while **APP_KEY** you can generate using **```php artisan generate:key```**
8. Run **```php artisan october:up```**, which will create the required tables.
	1. If you receive an error, then you can either follow that error or go onto the next step. Running **```php artisan october:up```** should prevent you from having to change the application URL and the MySQL connection.
9. Open up the following files and do the following where required:
	1. **config/app.php** - Change the application URL to the location of the CMS on the remote server.
	2. **config/cms.php** - Change the back-end URI prefix to admin (or anything that you prefer).
	3. **config/database.php** -- Change the MySQL connection to match the details of your database on the remote server.
10. If you have made the changes on local, upload them to the remote server and then ppen the CMS that is on remote server to enter the default login details at **https://yourdomain.com/yourbackendurl**:
- Username: **admin**
- Password: **admin**

### Getting Started and Plugins
The CMS has a few plugins, which allow you to manage pages and APIs, maintain the builder plugin, and setting CORS headers. You can install plugins either by uploading the plugins that came with the CMS for the EHU-Health app to the plugins folder or you can install plugins through the CMS directly.

You can manage the pages through the Pages tab.

### Create a page
To create a page, click on the "Create" button and fill in the following details:

- **Title** - The name of the page.
- **Slug** - The URL of the page, which is by default the same as the title.
- **Colour** - The colour of the page left border navigation bar and text underline.
- **Body** - The main content of the page.

Click either "Create or "Create and close" to complete.

### Read a page
To read a page, click on the page title or slug.

### Update a page
To update a page, click on the page title or slug and edit the following details:

- **Title** - The name of the page.
- **Slug** - The URL of the page, which is by default the same as the title.
- **Colour** - The colour of the page left border navigation bar and text underline.
- **Body** - The main content of the page.

Click either "Save or "Save and close" to complete.

### Delete a page
To delete a page, click on the checkbox that is next to the page title and slug that you would like to remove and then click on "Delete selected" to confirm the action.

### Reordering and grouping pages
To reorder and group pages, click on the "Reorder records" button. From there, you will be able to move each of the pages up and down by drag and drop functionality. To group pages, drag the page that you would like to be the child and drop that page on top of its parent page.

There is no need to save as that is done automatically for every drag and drop that you do.

### Pages API
To read the Pages API, visit https://yourdomain.com/api/v1/pages or whatever you have set the API to in the API Generator. The Pages API will return a JSON response of the pages that exist within the "Pages" tab.


### Managing the Pages plugin
To access the plugin's database, models, permissions, backend menu and controllers, click on on the available options on the left hand side of the builder page update. More information about how the builder plugin works can be accessed on the **[official builder plugin page](https://octobercms.com/plugin/rainlab-builder)** and on this **[YouTube tutorial](https://www.youtube.com/watch?v=O8f_MZS4E0I)**.

### CORS-Settings plugin
To access the CORS headers, go to the "Settings" tab and click on CORS-Settings. This is ideal for those that might not have full access to the server that the CMS is hosted on or do not have much technical knowledge on CORS. To set a CORS header, type in the frontend URL (For example, **https://yourfrontenddomain.com/**) in the "Allowed origins" tab and then click either "Save or "Save and close" to complete. Without this or setting it in the server's configuration, the frontend of the app will not be able to function.

## Support

Any questions, advice or require a copy of the live database, please contact David Coope at cooped@edgehill.ac.uk.
