<?php namespace DavidCoope\Pages\Models;

use Model;

/**
 * Model
 */
class Page extends Model
{
    use \October\Rain\Database\Traits\NestedTree;
    use \October\Rain\Database\Traits\Validation;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
       'title' => 'required|between:1,40',
       'slug' => 'required',
       'colour' => 'required',
       'body' => 'required'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'davidcoope_pages_';
}
