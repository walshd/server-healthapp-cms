<?php return [
    'plugin' => [
        'name' => 'Pages',
        'description' => '',
    ],
    'page' => [
        'title' => 'Title',
        'slug' => 'Slug',
        'colour' => 'Colour',
        'body' => 'Body',
        'manage_pages' => 'Manage Pages',
    ],
];